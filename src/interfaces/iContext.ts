import { SinonSpy } from 'sinon';

export interface IContext {
    continue: SinonSpy,
    complete: SinonSpy,
    catch: SinonSpy
}