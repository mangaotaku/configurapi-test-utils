import { IIdentity, IRequest } from 'configurapi';

export interface IMockHandler {
    execute(): Promise<void>;
    expectedHeaders( expected?: object ): IMockHandler;
    expectedRequest( expected?: Partial<IRequest> ): IMockHandler;
    expectedBody( expected?: string | object ): IMockHandler;
    expectedStatusCode( expected?: number ): IMockHandler;
    expectedIdentity( expected?: Partial<IIdentity> ): IMockHandler;
    expectedPayload( expected?: any ): IMockHandler;
    continued(): IMockHandler;
    completed(): IMockHandler;
    caught(): IMockHandler;
}