import { IEvent, IIdentity, IRequest, IResponse, Request, Response } from 'configurapi';

export class MockEvent implements IEvent {

    id: string;
    correlationId: string;
    params: {};
    name: string;
    method: string;
    request: IRequest;
    response: IResponse;
    payload: string | object;
    identity: IIdentity;

    constructor( event: Partial<IEvent> )
    {
        this.id = event?.id;
        this.correlationId = event?.correlationId;
        this.params = event?.params || {};
        this.name = event?.name;
        this.method = event?.method;
        this.request = event.request || new Request(undefined);
        this.response = event.response || new Response();
        this.payload = event.payload? event.payload: {};
        this.identity = event?.identity;
    }

    static fromJson(obj: any): MockEvent
    {
        if ( obj === undefined ) return undefined;
        return new MockEvent(obj);
    }

    resolve(str:string): string
    {
        return;
    }
}