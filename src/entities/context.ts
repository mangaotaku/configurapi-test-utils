import { spy } from 'sinon';

import { IContext } from '../interfaces/iContext';

export class Context {
    static create(): IContext {
        return {
            continue: spy(),
            complete: spy(),
            catch: spy()
        };
    }
}