import { assert } from 'chai';
import { IEvent, IIdentity, IRequest } from 'configurapi';

import { IContext } from '../interfaces/iContext';
import { IMockHandler } from '../interfaces/iMockHandler';
import { Context } from './context';
import { IllegalHandlerError } from './illegalHandlerError';
import { MockEvent } from './mockEvent';

export class MockHandler implements IMockHandler
{
    private context: IContext;
    private handler: Function;
    private args: [ Partial<IEvent>, ...any ];
    private tests: Array<( event: IEvent, context: IContext ) => void>;

    constructor( handler: Function, args: [ Partial<IEvent>, ...any ] = [ MockEvent.fromJson({}) ], context: IContext = Context.create() )
    {
        if ( !handler.name.endsWith('Handler') )
        {
            throw new IllegalHandlerError();
        }
        if ( !args )
        {
            args = [ MockEvent.fromJson({}) ];
        }
        else if( args[0] === undefined )
        {
            args[0] = MockEvent.fromJson({});
        }
        else
        {
            args[0] = MockEvent.fromJson(args[0]);
        }
        this.handler = handler;
        this.args = args;
        this.context = context;
        this.tests = [];
    }

    async execute(): Promise<void>
    {
        await this.handler.apply( this.context, this.args );

        for ( const test of this.tests )
        {
            test( this.args[0] as IEvent, this.context );
        }
    }

    static test( handler: Function, args?: [ Partial<IEvent>, ...any ] ): MockHandler
    {
        return new MockHandler( handler, args, Context.create() );
    }

    private deepEqualOrThrow( expected: any, actual: any, message?: string ): void
    {
        try
        {
            assert.deepEqual( expected, actual, message );
        }
        catch(e)
        {
            throw e;
        }
    }

    private equalOrThrow( expected: any, actual: any, message?: string ): void
    {
        try
        {
            assert.equal( expected, actual, message );
        }
        catch(e)
        {
            throw e;
        }
    }

    expectedHeaders( expected: object = {} ): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.deepEqualOrThrow( expected, event.response.headers, this.expectedHeaders.name ) );
        return this;
    }

    expectedRequest( expected: Partial<IRequest> ): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.deepEqualOrThrow( expected, event.request, this.expectedRequest.name ) );
        return this;
    }

    expectedBody( expected: any = '' ): IMockHandler
    {
        if ( typeof expected === 'string' )
        {
            this.tests.push( ( event: IEvent, context: IContext ) => this.equalOrThrow( expected, event.response.body, this.expectedBody.name ) );
        }
        else
        {
            this.tests.push( ( event: IEvent, context: IContext ) => this.deepEqualOrThrow( expected, event.response.body, this.expectedBody.name ) );
        }
        return this;
    }

    expectedStatusCode( expected: number ): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.equalOrThrow( expected, event.response.statusCode, this.expectedStatusCode.name ) );
        return this;
    }

    expectedIdentity( expected: Partial<IIdentity> ): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.deepEqualOrThrow( expected, event.identity, this.expectedIdentity.name ) );
        return this;
    }

    expectedParams( expected: any ): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.deepEqualOrThrow( expected, event.params, this.expectedParams.name ) );
        return this;
    }

    expectedPayload( expected: any ): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.deepEqualOrThrow( expected, event.payload, this.expectedPayload.name ) );
        return this;
    }

    continued(): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.equalOrThrow( context.continue.called, true, this.continued.name ) );
        return this;
    }

    completed(): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.equalOrThrow( context.complete.called, true, this.completed.name ) );
        return this;
    }

    caught(): IMockHandler
    {
        this.tests.push( ( event: IEvent, context: IContext ) => this.equalOrThrow( context.catch.called, true, this.caught.name ) );
        return this;
    }
}