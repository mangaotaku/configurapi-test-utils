export class IllegalHandlerError extends Error {
    constructor(msg: string = 'An api handler must end with \'Handler\'.')
    {
        super(msg);
    }
}