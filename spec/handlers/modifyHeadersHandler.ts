import { IEvent } from "configurapi";

export async function modifyHeadersHandler( event: IEvent, header: string ){
    event.response['headers'] = { header: header };
    return this.complete();
}