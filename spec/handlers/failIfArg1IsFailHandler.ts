import { ErrorResponse, IEvent } from 'configurapi';

export async function failIfArg1IsFailHandler(event: IEvent, arg1: string)
{
    event.payload['key'] = 'value';

    if ( arg1 === 'fail' )
    {
        event.response = new ErrorResponse('Bad arg1', 500);
        return this.complete();
    }
    else
    {
        event.response.statusCode = 204;
        return this.continue();
    }
}