import { IEvent } from "configurapi";

export async function setParamsObjectHandler( event: IEvent, object: any ){
    event.params['object'] = object;
    return this.complete();
}