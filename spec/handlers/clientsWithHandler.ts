import { ErrorResponse, IEvent } from 'configurapi';
import { JsonResponse } from 'configurapi-handler-json';

export async function clientsWithHandler(event: IEvent, clients: { client: { doSomething: () => Promise<number> } } )
{
    let number = await clients.client.doSomething();
    if ( number === 1337 ){
        event.response = new JsonResponse({number: number});
        return this.continue();
    }else{
        event.response = new ErrorResponse(number, number);
        return this.complete();
    }
}