import { IEvent } from "configurapi";

export async function setStatusCodeHandler( event: IEvent, statusCode: number ){
    event.response.statusCode = statusCode;
    return this.complete();
}