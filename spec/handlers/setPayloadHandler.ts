import { IEvent } from "configurapi";

export async function setPayloadHandler( event: IEvent, payload: any ){
    event.payload = payload;
    return this.complete();
}