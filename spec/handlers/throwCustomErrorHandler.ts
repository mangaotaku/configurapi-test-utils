import CustomError from "./customError";

export async function throwCustomErrorHandler() {
    throw new CustomError("error!");
}