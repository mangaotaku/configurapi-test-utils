import { IEvent } from "configurapi";

export async function setBodyHandler( event: IEvent, body: any ){
    event.response.body = body;
    return this.complete();
}