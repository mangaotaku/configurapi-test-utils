import { IEvent } from "configurapi";

export async function someHandlerIncorrectlyNamed( event: IEvent ) {
    event.response.statusCode = 204;
}