export default class CustomError extends Error {

    constructor(m:string) {
        super(m);
    }
};