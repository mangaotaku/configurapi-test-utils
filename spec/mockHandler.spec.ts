import { assert } from 'chai';
import { IClaim, Identity, Request } from 'configurapi';

import { IllegalHandlerError } from '../src/entities/illegalHandlerError';
import { MockHandler } from '../src/entities/mockHandler';
import { clientsWithHandler } from './handlers/clientsWithHandler';
import CustomError from './handlers/customError';
import { failIfArg1IsFailHandler } from './handlers/failIfArg1IsFailHandler';
import { modifyHeadersHandler } from './handlers/modifyHeadersHandler';
import { setBodyHandler } from './handlers/setBodyHandler';
import { setParamsObjectHandler } from './handlers/setParamsObjectHandler';
import { setPayloadHandler } from './handlers/setPayloadHandler';
import { setStatusCodeHandler } from './handlers/setStatusCodeHandler';
import { someHandlerIncorrectlyNamed } from './handlers/someHandlerIncorrectlyNamed';
import { throwCustomErrorHandler } from './handlers/throwCustomErrorHandler';

describe('MockHandler Tests', async () => {
    [
        { title: 'test headers boolean', value: true },
        { title: 'test headers boolean', value: false },
        { title: 'test headers string', value: 'string' },
        { title: 'test headers number', value: 12345 },
        { title: 'test headers object', value: { key: 'value' } },
    ].forEach( async (test: { title: string, value: any }) =>
        it( test.title, async () =>
            await MockHandler.test( modifyHeadersHandler, [ {}, test.value ] )
            .expectedHeaders({ header: test.value})
            .completed()
            .execute()
        )
    );

    [
        { title: 'test body boolean', value: true },
        { title: 'test body boolean', value: false },
        { title: 'test body string', value: 'string' },
        { title: 'test body number', value: 12345 },
        { title: 'test body object', value: { key: 'value' } },
    ].forEach( async (test: { title: string, value: any }) =>
        it( test.title, async () =>
            await MockHandler.test( setBodyHandler, [ {}, test.value ] )
            .expectedBody(test.value)
            .completed()
            .execute()
        )
    );

    [
        { title: 'test statusCode number', value: 12345 },
        { title: 'test statusCode number', value: undefined },
    ].forEach( async (test: { title: string, value: any }) =>
        it( test.title, async () =>
            await MockHandler.test( setStatusCodeHandler, [ {}, test.value ] )
            .expectedStatusCode(test.value)
            .completed()
            .execute()
        )
    );

    [
        { title: 'test identity no claims', identity: new Identity('id', [ <IClaim>{ type: 'type', value: 'value', scope: 'scope' } ]) },
        { title: 'test identity with claims', identity: new Identity('id') },
        { title: 'test identity as object', identity: { id: 'id' } }
    ].forEach( async (test: { title: string, identity: any }) =>
        it( test.title, async () =>
            await MockHandler.test( failIfArg1IsFailHandler, [ { identity: test.identity } ] )
            .expectedIdentity(test.identity)
            .continued()
            .execute()
        )
    );

    [
        { title: 'test payload boolean', value: true },
        { title: 'test payload boolean', value: false },
        { title: 'test payload string', value: 'string' },
        { title: 'test payload number', value: 12345 },
        { title: 'test payload object', value: { key: 'value' } },
    ].forEach( async (test: { title: string, value: any }) =>
        it( test.title, async () =>
            await MockHandler.test( setPayloadHandler, [ {}, test.value ] )
            .expectedPayload(test.value)
            .completed()
            .execute()
        )
    );

    [
        { title: 'test params object boolean', value: true },
        { title: 'test params object boolean', value: false },
        { title: 'test params object string', value: 'string' },
        { title: 'test params object number', value: 12345 },
        { title: 'test params object object', value: { key: 'value' } },
    ].forEach( async (test: { title: string, value: any }) =>
        it( test.title, async () =>
            await MockHandler.test( setParamsObjectHandler, [ {}, test.value ] )
            .expectedParams({object:test.value})
            .completed()
            .execute()
        )
    );

    [
        { title: 'test request boolean', value: new Request('') },
        { title: 'test request boolean', value: new Request('name') },
        { title: 'test request boolean', value: new Request(undefined) }
    ].forEach( async (test: { title: string, value: any }) =>
        it( test.title, async () =>
            await MockHandler.test( failIfArg1IsFailHandler, [ { request: test.value } ])
            .expectedRequest(test.value)
            .continued()
            .execute()
        )
    );

    it('test handler passes, extra args do not matter', async () => {
        await MockHandler.test( failIfArg1IsFailHandler, [ {}, 'one', 'two'])
                .continued()
                .execute()
    })

    it('test handler passes, no args', async () => {
        await MockHandler.test( failIfArg1IsFailHandler )
                .continued()
                .execute()
    })

    it('test handler completed', async () => {
        await MockHandler.test( failIfArg1IsFailHandler, [ {}, 'fail', 'two' ] )
                .expectedHeaders({
                    "Content-Type": "application/json"
                })
                .expectedBody({
                    details: "",
                    message: "Bad arg1",
                    statusCode: 500
                })
                .expectedStatusCode( 500 )
                .completed()
                .execute()
    })

    it('Test client calls', async () => {
        let mc = {
            client: {
                doSomething: () => Promise.resolve(1337)
            }
        }

        await MockHandler.test( clientsWithHandler, [ undefined, mc ])
            .expectedStatusCode(200)
            .expectedBody({number: 1337})
            .continued()
            .execute()
    })

    it('Test client calls', async () => {
        let mc = {
            client: {
                doSomething: () => Promise.resolve(12)
            }
        }

        await MockHandler.test( clientsWithHandler, [ undefined, mc ])
            .expectedStatusCode(12)
            .expectedHeaders({
                "Content-Type": "application/json"
            })
            .expectedBody({
                details: "",
                message: 12,
                statusCode: 12
            })
            .completed()
            .execute()
    })

    it('Test incorrect handler', async () => {
        try
        {
            await MockHandler.test( someHandlerIncorrectlyNamed ).execute();
            assert.isTrue(false);
        }
        catch(e)
        {
            assert.isTrue( e instanceof IllegalHandlerError );
        }
    })

    it('Test throw error handler', async () => {
        try
        {
            await MockHandler.test( throwCustomErrorHandler ).execute();
        }
        catch(e)
        {
            assert.isTrue( e instanceof CustomError );
        }
    })
})